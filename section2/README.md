## File operators (tests)
```bash
-d FILE True if file is a directory.
-e FILE True if file exists.
-f FILE True if file exists and is a regular file.
-r FILE True if file is readable by you.
-s File True if file exists and is not empty.
-w File True if the file is writable by you.
-x File True if the file is executable by you.
```

## String operators (tests)
```bash
-z STRING True if string is empty.
-n STRING True if string is not empty
STRING1 = STRING2 True if string are equal.
STRING1 != STRING2 True if the strings are not equal
```

## Arithmetic operators (tests)
```bash
arg1 -eq arg2 True if arg1 is equal to arg2.
arg1 -ne arg2 True if arg1 is not equal to arg2.

arg1 -lt arg2 True if arg1 is lessthan arg2.
arg1 -le arg2 True if arg1 is less than or equal to arg2.

arg1 -gt arg2 True if arg1 is greater than arg2.
arg1 -ge arg2 True if arg1 is greater than or equal to arg2.
```

## Positional Parameters
```bash
$ script.sh parameter1 parameter2 parameter3

$0: "script.sh"
$1: "parameter1"
$2: "parameter2"
$3: "parameter3"
```