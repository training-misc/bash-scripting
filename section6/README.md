## Wildcards
- A character or string sued for pattern matching.
- Globbing expands the wildcard pattern into a list of files and/or directories. (paths)
- Wildcards can be sued with most commands.
    - ls
    - rm
    - cp

- "*" - matches zero or more characters.
    - *.txt
    - a*
    - a*.txt
- "?" - matches exactly one character.
    - ?.txt
    - a?
    - a?.txt

## More Wildcards - Character Classes
- [] - A character class.
    - Matches any of the characters included between the brackets. Matcches exactly one character
    - [aeiou]
    - ca[nt]*
        - can
        - cat
        - candy
        - catch
- [!] - Matches any of the characters NOT included between the brackets. Matches exactly one character.
    - [!aeiou]*
    - baseball
    - cricket

# More Wildcards - Ranges
- Use two characters separated by a hypen to create a range in a character class.
- [a-g]*
    - Matches all file that start with a, b, c, d, e, f, or g.
- [3-6]*
    - Matches all files that start with 3, 4, 5, or 6.


## Names Character Classes
- [[:alpha:]] - Matches alphabeth upper and lower cases
- [[:alnum:]] - Matches alpha numberic upper and lower cases
- [[:digit:]] - Matches numbers from 0 to 9
- [[:lower:]] - Matches lower case letter
- [[:spaces:]] - Matches any white space
- [[:upper:]] - Matches any upper case letter

## Matching Wildcard patterns
- \ - escape character. Use if you want to match a wildcard character.
    - Match all files that end with a question mark:
        - *\?
            - done?