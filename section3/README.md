## Exit Status/ Return Code
- Every command returns an exit status
- Range from 0 to 255
- 0 = success
- Other than 0 = error condition
- Use for error checking
- Use man or info to find meaning of exit status

## && and ||
- && = AND
`mkdir /tmp/back && cp test.txt /tmp/back/`
First command needs to be true before executing the the second command
- || = OR
cp test.tx /tmp/bak/ || cp test.txt /tmp
If first command is true, then second command does not need to run. But if first command is false, then second command executes.

## The semicolon
- Separate commands with a semicolon to ensure they all get executed.

```
cp test.txt /tmp/bak/ ; cp test.txt /tmp
```
### Same as:
```
cp test.txt /tmp/bak/
cp test.txt /tmp
```

## Exit Command
- Explicitly define the return code
    - exit 0
    - exit 1
    - exit 2
    - exit 255
    - etc...
- The default value is that of the last command executed.

