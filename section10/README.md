## Why Debug?
- A bug is really an error.
- Examine the inner workings of your script.
- Determine the root of unexpected behavior.
- Fix bugs (errors).

## Build in Debugging Help
- -x = Prints commands as they execute
- After substitutions and explansions
- Called an x-trace, tracing, or print debugging
- #!/bin/bash -x
- set -x
    - set +x to stop debugging
- -e = Exit on error.
- Can be combined with other options.
    - #!/bin/bash -ex
    - #!/bin/bash -xe
    - #!/bin/bash -e -x
    - #!/bin/bash -x -e
- -v = Prints shell input lines as they are read.
- Can be combined with other options.

## For more information
- help set | less

## Manual Debugging
- You can create your own debugging code.
- Use a special variable like DEBUG
    - DEBUG=true
    - DEBUG=false

## Manual Copy and Paste
- Open up a second terminal.
- Copy and paste the commands into the terminal.
- Can be helpful to use "set-x" on the command line.

## Syntax Highlighting
- Syntax errors are common.
- Typos, missing brackets, missing quotes, etc.
- Use an editor with syntax highlighting.
    - vi/vim
    - emacs
    - nano
    - gedit
    - kate
    - geany

## PS4
- Controls what is displayed before a line when using the "-x" option.
- The default value is "+"
- Bash Variables
    - BASH_SOURCE, LINENO, etc

`PS4='+ $BASH_SOURCE: $LINENO '`

## DOS vs Linux(Unix) File Types
- CRLF/Carriage Return, Line Feed
- cat -v script.sh

```bash
#!/bin/bash^M
# This file contains carriage returns. ^M
echo "Hello world."^M
```
- file script.sh
    - script.sh:Bourne-Again shell script, ASCII text executable, with CRLF line terminators
- dos2unix script.sh
- file script.sh
    - script.sh: Bourne-Again shell script, ASCII text executable

## How does this happen?
- Using a Windows editor and uploading to Linux
    - Some editors can be configured to use just LF
- Pasting from Windows into a Linux terminal
- Pasting from a web browser into a terminal
