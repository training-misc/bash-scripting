#!/bin/bash -x
PS4='+ ${BASH_SOURCE} : ${LINENO} :${FUNCNAMe[0]}() '

debug() {
    echo "Executing: $@"
    $@
}
debug ls