## While Loop Format
```bash
while [ CONDITION_IS_TRUE ]
do
    command 1
    command 2
    command N
done
```

## Infinite Loops
```bash
while true
do
    command N
    sleep 1
done
```