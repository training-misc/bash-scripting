#!/bin/bash
FS_NUM=1
grep xfs /etc/fstab | while read FS MP REST
do
    echo "${FS_NUM}: file system: ${FS}"
    echo "${FS_NUM}: mount point: ${MP}"
    ((FS_NUM++))
done

```
1: file system: /dev/mapper/centos-root
1: mount point: /
2: file system: LABEL=boot
2: mount point: /boot
```