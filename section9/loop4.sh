#!/bin/bash
LINE_NUM=1
while read LINE
do
    echo "${LINE_NUM}: ${LINE}"
    ((LINE_NUM++))
done < /etc/fstab

```
# /etc/fstab
#
/dev/mapper/centos-root / xfs defaults 1 1
LABEL=boot /boot          xfs defaults 1 2
/dev/mapper/centos-swap swap swap defaults 00
```