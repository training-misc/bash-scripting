## Creating a function

```bash
function function-name() {
    # Code goes here.
}

function-name() {
    # Code goes here.
}

```

## Positional Prameters
- Functions can accept parameters.
- The first parameter is stored in $1.
- The second paramter is stored in $2, etc.
- $@ contains all of the parameters
- just like shell scripts.
    - $0 = the script itself, not function name.

## Variable Scope
- By default, variables are global.
- Variables have to be defined before used.

```bash
GLOBAL_VAR=1
# GLOBAL_VAR is available
# in the function.
my_function
```

```bash
# GLOBAL_VAR is NOT available
# in the function
my_function
GLOBAL_VAR=1
```

## Local Variables
- Can only be accessed within the function
- Create using the local keyword
    - local LOCAL_VAR=1
- Only functions can have local variables.
- Best practice to keep variables local in functions

## Exit Status (Return Codes)
- Functions have an exit status
- Explicitly
    - return <RETURN_CODE>
- Implicity
    - The exit status of the last command executed in the function
- Valid exit codes range from 0 to 255
- 0 = seuccess
- $? = the exit status

```bash
my_function
echo $?
```