## Case Statements
- Alternative to if statements
    - if [ "$VAR" = "one" ]
    - elif [ "$VAR" = "two" ]
    - elif [ "$VAR" = "three" ]
    - elif [ "$VAR" = "four" ]
- May be easier to read than complex if statements.
